package org.example.patterns.creational.singleton;

public class Main {
    public static void main(String[] args) {

        EmailService emailService1 = EmailService.getInstance("helo");

        Thread threadOne = new Thread(() -> {EmailService es = EmailService.getInstance("ASD");});

        Thread threadTwo = new Thread(() -> {EmailService es = EmailService.getInstance("ASD123");});

        threadOne.start();
        threadTwo.start();
    }
}
