package org.example.patterns.creational.singleton;

public class EmailService {

    private static volatile EmailService instance;
    private String data;

    private EmailService(String data){
        this.data = data;
    }

    public static EmailService getInstance(String data) {

        if (instance == null){
            synchronized (EmailService.class) {
                if (instance == null) {
                    instance = new EmailService(data);
                }
            }
        }

        return instance;
    }
}

