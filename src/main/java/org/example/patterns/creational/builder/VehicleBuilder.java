package org.example.patterns.creational.builder;

public interface VehicleBuilder {
    VehicleBuilder id(Long id);
    VehicleBuilder brand(String brand);
    VehicleBuilder model(String model);
    VehicleBuilder price(Integer price);
    VehicleBuilder color(String color);
}
