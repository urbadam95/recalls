package org.example.patterns.creational.builder;

public class CarBuilder implements VehicleBuilder{
    private Long id;
    private Integer price;
    private String brand;
    private String model;
    private String color;

    @Override
    public CarBuilder id(Long id) {
        this.id = id;
        return this;
    }

    @Override
    public CarBuilder brand(String brand) {
        this.brand = brand;
        return this;
    }

    @Override
    public CarBuilder model(String model) {
        this.model = model;
        return this;
    }

    @Override
    public CarBuilder price(Integer price) {
        this.price = price;
        return this;
    }

    @Override
    public CarBuilder color(String color) {
        this.color = color;
        return this;
    }

    public Car build() {
        return new Car(id, price, brand, model, color);
    }
}
