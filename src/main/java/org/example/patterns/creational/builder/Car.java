package org.example.patterns.creational.builder;

public class Car {
    private Long id;
    private Integer price;
    private String brand;
    private String model;
    private String color;

    public Car(Long id, Integer price, String brand, String model, String color) {
        this.id = id;
        this.price = price;
        this.brand = brand;
        this.model = model;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", price=" + price +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
