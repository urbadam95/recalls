package org.example.patterns.creational.builder;

public class Main {
    public static void main(String[] args) {
        CarBuilder carBuilder = new CarBuilder();
        Car car = carBuilder.price(10).brand("ASD").color("blue").build();

        System.out.println(car.toString());
    }
}
