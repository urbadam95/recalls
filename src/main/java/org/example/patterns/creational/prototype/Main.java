package org.example.patterns.creational.prototype;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        Owner owner = new Owner();
        owner.setName("Pista");

        Owner anotherOwner = new Owner();
        anotherOwner.setName("Ferko");

        TV tv = new TV();
        tv.setModel("LK2111");
        tv.setOwner(owner);

        TV copyTv = tv.clone();
        copyTv.getOwner().setName("Ferko");

        System.out.println(tv.getModel());
        System.out.println(copyTv.getModel());

        System.out.println(tv.getOwner().getName());
        System.out.println(copyTv.getOwner().getName());
    }
}
