package org.example.patterns.creational.prototype;

import java.util.Date;

public class TV implements Cloneable {

    public enum Brand {
        LG,
        SAMSUNG,
        SONY,
        PANASONIC
    }

    private int price;
    private String model;
    private Date releaseDate;
    private Owner owner;

    public TV() {
    }

    @Override
    protected TV clone() throws CloneNotSupportedException {
        try {
            TV tv = (TV) super.clone();
            tv.owner = owner.clone();
            return tv;
        } catch (CloneNotSupportedException ex) {
            throw new AssertionError();
        }
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}
