package org.example.patterns.creational.prototype;

public class Owner implements Cloneable {

    private String name;
    private int age;

    public Owner() {
    }

    @Override
    protected Owner clone() throws CloneNotSupportedException {
        try {
            return (Owner) super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new AssertionError();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
