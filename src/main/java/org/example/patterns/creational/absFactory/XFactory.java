package org.example.patterns.creational.absFactory;

public class XFactory extends Company {
    @Override
    public Shirt createShirt() {
        return new XShirt();
    }

    @Override
    public Jeans createJeans() {
        return new XJeans();
    }
}
