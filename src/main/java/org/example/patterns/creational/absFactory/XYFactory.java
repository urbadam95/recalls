package org.example.patterns.creational.absFactory;

public class XYFactory extends Company {
    @Override
    public Shirt createShirt() {
        return new XYShirt();
    }

    @Override
    public Jeans createJeans() {
        return new XYJeans();
    }
}