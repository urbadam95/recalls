package org.example.patterns.creational.absFactory;

public class Main {
    public static void main(String[] args) {
        Company x = new XFactory();
        Shirt xShirt = x.createShirt();
        Jeans xJeans = x.createJeans();
        System.out.println(xShirt.getClass() + "   " + xJeans.getClass());

        Company xy = new XYFactory();
        Shirt xyShirt = xy.createShirt();
        Jeans xyJeans = xy.createJeans();

        System.out.println(xyShirt.getClass() + "   " + xyJeans.getClass());
    }
}
