package org.example.patterns.creational.absFactory;

public class XShirt implements Shirt {
    @Override
    public void tryOn() {
        System.out.println("Gut");
    }
}
