package org.example.patterns.creational.absFactory;

public interface Jeans {
    void tryOn();
}
