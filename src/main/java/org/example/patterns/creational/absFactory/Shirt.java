package org.example.patterns.creational.absFactory;

public interface Shirt {
    void tryOn();
}
