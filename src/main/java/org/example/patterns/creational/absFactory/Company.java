package org.example.patterns.creational.absFactory;

public abstract class Company {
    public abstract Shirt createShirt();
    public abstract Jeans createJeans();
}
