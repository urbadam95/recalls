package org.example.patterns.creational.absFactory;

public class XYShirt implements Shirt {
    @Override
    public void tryOn() {
        System.out.println("Gut");
    }
}
