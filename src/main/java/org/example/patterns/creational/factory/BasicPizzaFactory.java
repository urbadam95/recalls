package org.example.patterns.creational.factory;

public class BasicPizzaFactory extends PizzaFactory {
    @Override
    public Pizza create() {
        return new BasicPizza();
    }
}
