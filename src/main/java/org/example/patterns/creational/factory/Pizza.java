package org.example.patterns.creational.factory;

public interface Pizza {
    void prepare();
}
