package org.example.patterns.creational.factory;

public abstract class PizzaFactory {

    public Pizza orderPizza() {
        Pizza pizza = create();
        pizza.prepare();
        return pizza;
    }

    public abstract Pizza create();
}
