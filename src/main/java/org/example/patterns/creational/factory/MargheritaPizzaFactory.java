package org.example.patterns.creational.factory;

public class MargheritaPizzaFactory extends PizzaFactory {
    @Override
    public Pizza create() {
        return new MargheritaPizza();
    }
}
