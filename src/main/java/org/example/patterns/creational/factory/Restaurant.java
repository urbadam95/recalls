package org.example.patterns.creational.factory;

public class Restaurant {
    public static void main(String[] args) {
        PizzaFactory factory = new BasicPizzaFactory();
        Pizza pizza = factory.create();
        pizza.prepare();
        System.out.println(pizza.getClass());
    }
}
