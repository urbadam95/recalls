package org.example.patterns.structural.flyweight;

public interface Animal {

  String getAnimalType();
  int[] getLocation();
  void setLocation(int latitude, int longitude);

}
