package org.example.patterns.structural.composite;

public interface Book {
    void checkout();
    void returnBook();
}
