package org.example.patterns.structural.adapter;

public class Adapter implements City {

    private AsianCity asianCity;

    public Adapter(AsianCity city) {
        this.asianCity = city;
    }

    @Override
    public String getName() {
        return this.asianCity.getName();
    }

    @Override
    public double getTemperature() {
        return (this.asianCity.getTemperature() * 1.8 ) + 32;
    }

    @Override
    public String getTemperatureScale() {
        return this.asianCity.getTemperatureScale();
    }

    @Override
    public boolean getHasWeatherWarning() {
        return this.asianCity.getHasWeatherWarning();
    }

    @Override
    public void setHasWeatherWarning(boolean hasWeatherWarning) {
        this.asianCity.setHasWeatherWarning(hasWeatherWarning);
    }
}
