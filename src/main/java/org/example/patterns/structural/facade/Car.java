package org.example.patterns.structural.facade;

public class Car {

  public static void main(String[] args) {
    CarFacade facade = new CarFacade();
    facade.drive();
  }

}
