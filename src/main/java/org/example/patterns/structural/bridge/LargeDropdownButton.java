package org.example.patterns.structural.bridge;

public class LargeDropdownButton extends DropdownButton {

  public void draw() {
    System.out.println("Setting size to large...");
    super.draw();
  }

}
