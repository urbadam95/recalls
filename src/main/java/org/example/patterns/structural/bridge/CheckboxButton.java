package org.example.patterns.structural.bridge;

public class CheckboxButton extends Button {

  private String size;
  
  public CheckboxButton(String size) {
    this.size = size;
  }

  public void draw() {
    System.out.println("Drawing a " + size +" checkbox button.\n");
  }

}
