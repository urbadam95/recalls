package org.example.patterns.structural.bridge;

public abstract class Button {

  abstract void draw();

}
