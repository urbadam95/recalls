package org.example.patterns.structural.bridge;

public class Canvas {

  public static void main(String[] args) {

    CheckboxButton checkboxButton = new CheckboxButton("medium");
    checkboxButton.draw();

    RadioButton radioButton = new MediumRadioButton();
    radioButton.draw();

    DropdownButton dropdownButton = new LargeDropdownButton();
    dropdownButton.draw();


  }

}
