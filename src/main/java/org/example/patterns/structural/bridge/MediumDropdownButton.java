package org.example.patterns.structural.bridge;

public class MediumDropdownButton extends DropdownButton {

  public void draw() {
    System.out.println("Setting size to medium...");
    super.draw();
  }

}
