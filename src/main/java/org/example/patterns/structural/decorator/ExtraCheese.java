package org.example.patterns.structural.decorator;

import java.util.ArrayList;

public class ExtraCheese implements Pizza {

    private ArrayList<String> toppings;
    private Pizza extraCheesePizza;

    public ExtraCheese(Pizza pizza) {
        this.extraCheesePizza = pizza;
        this.toppings = pizza.getToppings();
        this.toppings.add("Extra cheese");
    }

    @Override
    public ArrayList getToppings() {
        return this.extraCheesePizza.getToppings();
    }

    @Override
    public String getName() {
        return this.extraCheesePizza.getName();
    }
}
