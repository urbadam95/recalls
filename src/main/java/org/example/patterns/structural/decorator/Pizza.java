package org.example.patterns.structural.decorator;

import java.util.ArrayList;

public interface Pizza {

  ArrayList getToppings();
  String getName();

}
