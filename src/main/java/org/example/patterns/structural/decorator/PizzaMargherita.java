package org.example.patterns.structural.decorator;

import java.util.ArrayList;

public class PizzaMargherita implements Pizza {

  ArrayList<String> toppings = new ArrayList<>();
  String name = "Margherita";

  public PizzaMargherita() {
    toppings.add("cheese");
    toppings.add("tomato");
  }


  public ArrayList<String> getToppings() {
    return toppings;
  }

  public String getName() {
    return name;
  }



}
