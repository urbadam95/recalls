package org.example.patterns.structural.decorator;

public class Main {

  public static void main(String[] args) {
    order(new PizzaMargherita());
    order(new PizzaHawaiian());
    order(new PizzaPepperoni());
    order(new ExtraCheese(new PizzaPepperoni()));
    order(new ExtraCheese(new PizzaHawaiian()));
    order(new ExtraCheese(new PizzaMargherita()));
  }

  public static void order(Pizza pizza) {
    System.out.println("You have ordered a " + pizza.getName() +
        " pizza. The toppings are " + pizza.getToppings() + ".");
  }

}
