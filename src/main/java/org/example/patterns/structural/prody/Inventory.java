package org.example.patterns.structural.prody;

import java.util.ArrayList;

public interface Inventory {

  ArrayList<Item> getInventory();

}
